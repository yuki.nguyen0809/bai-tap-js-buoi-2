// Bài 1: Tiền lương nhân viên
// Đầu vào: 
// - Lương 1 ngày:
// - Số ngày làm: 
// Đầu ra:
// - Tổng lương=lương 1n*số ngày làm
function tinhTienLuong() {

    var tienCongNgayValue = document.getElementById("txt-tien-cong-ngay").value * 1;
    var tongSoNgayValue = document.getElementById("txt-tong-ngay").value * 1;

    var tienPhaiThanhToan = (tienCongNgayValue * tongSoNgayValue); 
    document.getElementById("result").innerHTML = `<div class="card text-white bg-primary">
      <div class="card-body">
            <h2 class="text-left"> ${new Intl.NumberFormat('vn-VN', { style: 'currency', currency: 'VND' }).format(tienPhaiThanhToan)}</h2>
    </div>`;
}


// Bài 2: Tính giá trị trung bình
// Đầu vào: 
// - 5 số thực bất kì
// Đầu ra:
// - Tổng 5 số /5 

function tinhTrungBinh() {

    var soThu1Value = document.getElementById("so-thu-1").value * 1;
    var soThu2Value = document.getElementById("so-thu-2").value * 1;
    var soThu3Value = document.getElementById("so-thu-3").value * 1;
    var soThu4Value = document.getElementById("so-thu-4").value * 1;
    var soThu5Value = document.getElementById("so-thu-5").value * 1;

    var trungBinhCong = (soThu1Value + soThu2Value + soThu3Value + soThu4Value + soThu5Value)/5; 
    document.getElementById("result2").innerHTML = `<div class="card text-white bg-success">
      <div class="card-body">
            <h2 class="text-left"> ${trungBinhCong}</h2>
    </div>`;
}



// Bài 3: Quy doi tien
// Đầu vào: 
// - USD: 
// Đầu ra:
// - VND: 23500*USD

function tienQuyDoi() {

    var usdValue = document.getElementById("usd").value * 1;
    var tienQuyDoi = usdValue*23500; 
    document.getElementById("result3").innerHTML = `<div class="card text-white bg-info">
      <div class="card-body">
            <h2 class="text-left"> ${new Intl.NumberFormat('vn-VN', { style: 'currency', currency: 'VND' }).format(tienQuyDoi)} </h2>
                </div>`;
}


// Bài 4: Tinh dien tich, chu vi HCN
// Đầu vào: 
// - CD: 6cm;
// - CR: 4cm;
// Đầu ra:
// - S=CD*CR
// - CV= (CD+CR)*2

function tinhChuVi() {

    var daiValue = document.getElementById("dai").value * 1;
    var rongValue = document.getElementById("rong").value * 1;

    var chuVi = (daiValue+rongValue)*2; 
    document.getElementById("result4").innerHTML = `<div class="card text-white bg-dark">
      <div class="card-body">
            <h2 class="text-left"> ${(chuVi)}</h2>
    </div>`;
}


// Bài 5: Tinh tong 2 ky so
// Đầu vào: 
// - num1: số có 2 chữ số 
// Đầu ra:
// -Ket qua = tổng 2 chữ số

function tinhTong() {

    var numValue = document.getElementById("so-2-chu-so").value * 1;
    var tenValue = Math.floor(numValue / 10 % 10);
    var unitValue = numValue % 10;

    var tinhTong = (tenValue + unitValue); 
    document.getElementById("result5").innerHTML = `<div class="card text-white bg-warning">
      <div class="card-body">
            <h2 class="text-left"> ${(tinhTong)}</h2>
    </div>`;
}
